Welcome to Priority Zero Gaming!
################################

.. _Community: https://priorityzerogaming.com
.. _Forums: https://forums.priorityzerogaming.com/
.. _Support Portal: https://forums.priorityzerogaming.com/support/
.. _Ideas Portal: https://forums.priorityzerogaming.com/ideas/
.. _Bug Tracker: https://forums.priorityzerogaming.com/bugs/
.. _Rule Discussions: https://forums.priorityzerogaming.com/rulediscussions/

**The enforcement is separated into two categories:**

* **Rules** - Rules of the community/server, these are Administrator enforced and do result in warnings, ejects, kicks and bans from the Priority Zero Gaming Network & Servers. Rules will **always** override Laws.
* **Laws** - Laws which are provided for your information and should be abided by, failure to do so will result with in-character law enforcement action. Severe breaking of laws may result in Administrator intervention.

.. note::

    Want to discuss, add, modify or remove one of our rules? Discuss it with the community.
    https://forums.priorityzerogaming.com/rulediscussions/ 

**Common Definitions:**

* **Player**: A player is defined as the OOC controller of a character. A player may have more than one character. Players are subject to OOC rules. Players must always remain OOC, and must keep their OOC emotions from mixing into IC character.
* **Character**: A character is defined the playable character whose actions are directly controlled by the Player. Other names for this may be avatar. Characters are subject to IC rules and laws.
* **Megagaming**: See roleplay rules.
* **Powergaming**: See roleplay rules.

Useful Links
==================

* `Forums`_
* `Support Portal`_
* `Ideas Portal`_
* `Bug Tracker`_
* `Rule Discussions`_

Use the sidebar for navigation.

.. toctree::
   :numbered:
   :maxdepth: 2
   :caption: Priority Zero Gaming Rules:

   rules/forums
   rules/lifegator

.. toctree::
   :numbered:
   :maxdepth: 2
   :caption: [SL] Woodland County Rules:

   rules/woodlandcounty/welcome
   rules/woodlandcounty/general
   rules/woodlandcounty/roleplay
   rules/woodlandcounty/avatars
   rules/woodlandcounty/rezrights
   rules/woodlandcounty/rentals
   rules/woodlandcounty/vehicles
   rules/woodlandcounty/faction
   rules/woodlandcounty/illegal

.. toctree::
   :numbered:
   :maxdepth: 2
   :caption: [FiveM] Los Santos Roleplay Rules:
   
   rules/fivem/general
   
.. toctree::
   :maxdepth: 2
   :caption: Other
   
   rules/glossary
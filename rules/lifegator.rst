###############
Lifegator Rules
###############

Additional Rules
================
Your usage of Lifegator is subject to the same rules as per the server you are in and those below.

I.E. If your character is in Woodland County, you are subject to all the Woodland County rules.

Always Roleplay
===============
All players must remain in character at all times on Lifegator. Lifegator is a 100% in character social network for the Priority Zero Gaming Network.

General Usage
=============
#. Do not troll, flame, spam, post meaningless content such as single word or off-topic replies, melodramatic or bewailing content or participate in otherwise disruptive behavior.
#. You may not keep a CK'd character active, nor continue to post from their account. Please refrain from "Their brother is posting on their account!", except in the situation where it may be someone posting once to inform followers and friends of the page of a memorial service.
#. Posting excessive explicit content should be avoided.

Security
========
#. Any attempt to breach another users account without their permission or knowledge will be treated similarly to that of attempting to breach their in-game account.
#. You may not OOCly use any form of VPN or proxy to bypass restrictions on your IP or accounts.
#. You may not OOCly impersonate another member of the community on Lifegator, including staff or other members, and may not harass other members OOCly.
#. All passwords are stored encrypted in the database - staff members and site administration do not have access to this information, and will never ask you for this information. Your password is your responsibility, do not give it to anyone.

General Conditions
==================
#. We reserve the right to modify or terminate the Lifegator service for any reason, without notice at any time.
#. We reserve the right to alter these Terms of Use at any time. If the alterations constitute a material change to the Terms of Use, we will notify you via internet mail according to the preference expressed on your account. What constitutes a "material change" will be determined at our sole discretion, in good faith and using common sense and reasonable judgement.
#. We reserve the right to refuse service to anyone for any reason at any time.
#. We may, but have no obligation to, remove Content and accounts containing Content that we determine in our sole discretion are unlawful, offensive, threatening, libellous, defamatory, obscene or otherwise objectionable or violates any party's intellectual property or these Terms of Use.
#. Lifegator service makes it possible to post images and text hosted on Lifegator to outside websites. This use is accepted (and even encouraged!). However, pages on other websites which display data hosted on Lifegator must provide a link back to Lifegator.

Copyright
=========
#. We claim no intellectual property rights over the material you provide to the Lifegator service. Your profile and materials uploaded remain yours. You can remove your profile at any time by deleting your account. This will also remove any text and images you have stored in the system.
#. We encourage users to contribute their creations to the public domain or consider progressive licensing terms.

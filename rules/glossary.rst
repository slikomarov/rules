#######################
Glossary & Common Terms
#######################

.. note::

  This is a glossary of common terms and words which are used throughout all Priority Zero Gaming, some terms may be specific to a related game.
  
.. _Community: https://priorityzerogaming.com
.. _Forums: https://forums.priorityzerogaming.com/
.. _Support Portal: https://forums.priorityzerogaming.com/support/
.. _Ideas Portal: https://forums.priorityzerogaming.com/ideas/
.. _Bug Tracker: https://forums.priorityzerogaming.com/bugs/
.. _Rule Discussions: https://forums.priorityzerogaming.com/rulediscussions/

SL
  Second Life

Soon
  Soon does not imply any particular date, time, decade, century, or millennia in the past, present, and certainly not the future. Soon shall make no contract or warranty between Priority Zero Gaming and the end user. Soon will arrive some day, Priority Zero Gaming does guarantee that soon will be here before the end of time. Maybe. Do not make plans based on soon as Priority Zero Gaming will not be liable for any misuse, use, or even casual glancing at soon.
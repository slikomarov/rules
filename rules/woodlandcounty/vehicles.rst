#############
Vehicle Rules
#############
.. note::

  These are the rental rules which are in place to maintain order and stability in the server.

.. _Upper Administration Team: https://forums.priorityzerogaming.com/support/
.. _Administration Team: https://forums.priorityzerogaming.com/support/
.. _Development Team: https://forums.priorityzerogaming.com/support/
.. _Support Portal: https://forums.priorityzerogaming.com/support/
.. _idea tracker: https://forums.priorityzerogaming.com/ideas/
.. _bug tracker: https://forums.priorityzerogaming.com/bugs/

Driving & Road Rules
====================
All drivers are expected to follow the posted road regulations while on the roads of Woodland County is to the right side of the road.

All laws and road rules in the State of Vermont, USA are to be adhered to and can be subject to police interaction.

Failure to do so may result in IC police interaction.

Always In-Character
-------------------
If you are driving, regardless of OOC tags, you are considered in-character and can be subject to interaction with police, 
other drivers and all others currently in-character on sim. Otherwise, don't drive.

Driver's License
================
Driver's licenses are required by state to operate a vehicle. The right license must be held by the operator in accordance with state law.

Upon request by the appropriate authority, you must provide a physical texture of your driver's license. If you do not have a texture, you do not have a license.

Licenses can be aquired by the appropriate form. 

.. todo:: Add link for form.

There is no OOC punishment for failure to have a license and will be handled by IC law enforcement.

Out of State License
--------------------
Out of State Licenses are permitted and are handled accordingly. 

Upon request by the appropriate authority, you must provide a physical texture of your driver's license.

Vehicle Registration
====================
Valid vehicle registration is required by state to operate a vehicle. The right registration plate must be displayed on the vehicle in accordance with state law.

Registration can be aquired by the appropriate form.

.. todo:: Add link for form.

There is no OOC punishment for failure to have a license and will be handled by IC law enforcement.

"Default Plates" aka. Trader Plates
-----------------------------------
Default plates are described as any plate that comes with a vehicle as stock. Notably these vehicles are SZYM & Pro Street Cars.

These plates are deemed as "Trader Plates" and are handled as such in accordance with state law.

Fees
====
As driver's licenses and registration plates are critical in roleplay, these are needed for operators and vehicles before being driven. As such, a texture is required and a cost is associated with this.

The cost of a Player's first license & registration is covered by the sim.

Additional licenses and registration plates are subject to a L$25 processing fee to minimise abuse, cover time and upload fees.

Restricted Vehicles & Modifications
===================================
The following vehicles are restricted from use unless approved by the `Administration Team`_

#. Military Vehicles (including tanks, armoured vehicles, reconnaissance vehicle, etc.)
#. Aircraft Vehicles (including helicopters, light air wing, commercial jets, etc.)
#. Jetpacks

:Potential Punishment: 3 Day Suspension

Law Enforcement Vehicles
------------------------
Law enforcement who are operating in the course of their duties are exempt from the above.

External Emergency Vehicles
---------------------------
Emergency vehicles which are not based within Woodland County are permitted, however are subject to state laws and may be prosecuted for impersonation.

Emergency Lightning
-------------------
Emergency lightning on vehicles are permitted, however are subject to state laws and may be prosecured for impersonation.

Department Policies & Regulations
---------------------------------
Official department heads may enforce internal policies regarding personally owned vehicles (POV), emergency lighting, take own vehicles, etc. these are to be followed, or may result in IC reprecussions.
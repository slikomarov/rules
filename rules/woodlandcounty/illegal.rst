#############
Illegal Rules
#############
.. note::
 The Illegal Rules are set up due to the high amount of criminal roleplayers, to limit total chaos, and somehow create a market. Money should flow and allowing only certain people to do specific actions can ensure that. We also aim to let people venture in new kinds of roleplay, which they didn't do before, sadly this has to be done by setting limitations, sometimes.

.. _Upper Administration Team: https://forums.priorityzerogaming.com/support/
.. _Administration Team: https://forums.priorityzerogaming.com/support/
.. _Development Team: https://forums.priorityzerogaming.com/support/
.. _Support Portal: https://forums.priorityzerogaming.com/support/
.. _idea tracker: https://forums.priorityzerogaming.com/ideas/
.. _bug tracker: https://forums.priorityzerogaming.com/bugs/

Restricted Areas
================
You may not commit serious crimes on purpose in high profile areas without adequate law enforcement on duty. You may check if there are enough law enforcement by asking an administrator. This limitation, however, is not to be used as a "safe zone". If you are being chased by an attacker and run onto the steps of a police station, this does not mean they must suddenly stop all illegal activity. The following zones include all of their reasonable surrounding perimiters.

All restricted areas may have crimes purposefully committed on them such as robberies of banks with permission from the `Administration Team`_.

Examples:

* Government Buildings such as Town Hall, Memorials, Court Houses, Court Offices, and similar Facilities
* Lightning Hospital, or similar Medical Facilities
* Woodland County Sheriff's Department, Stations, and Facilities
* Woodland County Fire & Rescue Department, Stations, and Facilities
* Department of Public Safety HQ & Facilities 
* Bank of Woodland

Arson
=====
Comitting arson requires that an adequate number of emergency service employees are available to respond to the fire. All arson requires the owner of the property to be online when the request is made to administrators.

Note: It is assumed that all gas stations & government buildings have fire detection systems.

Small Fires
-----------
Vehicles, small buildings without anyone inside, sheds, and similar places may be targeted with arson without firefighters available assuming the fire remains contained.

Large Fires
-----------
Large buildings, buildings with people inside, forest fires, and similar places may be targeted with arson provided there are at least two firefighters available, or three or more if rescue is involved.

Kidnapping
===========
If planning to leave a character to starve to death or killed, one must have a CK application accepted on the character. Otherwise there must be roleplay intended to free/feed the character.

Property Break-Ins, Robberies & Theft
=====================================
If administrative intervention is necessary to complete a robbery or theft, such as unlocking a door that was kicked in, it must adhere to the following rules and needs `Administration Team`_ approval. If a door was left open for example and you just walked in, you do not need to follow the rules below or approval. An administrator is needed if property break-ins are done through the use of a door ram/lockpick and you are not law enforcement.

You may not rob / steal:

* If you have less than 10 hours on your character.
* Other characters with less than 5 hours.
* On-duty law enforcement officer's equipment without administraive permission.
* Faction badges or identification cards.
* Private custom textures (unless the player agrees).
* More than $5,000 from someone's bank account utilizing their stolen bank card (unless the player agrees).
* Property or vehicle keys (unless the player agrees).
* Driver's license or vehicle plates (unless the player agrees).
  
Some pieces of property require `Administration Team`_ Approval to break into. They are:

* Evidence Storage
* Ammunations
* Banks

Residential Property
--------------------
All private residential property may be broken into and burglarized at any time except when the house is expecitily deemed OOC and the owner of the property is offline.

When the request is made at least two law enforcement officers must be available to respond. Residential refers to a style of property that is designed for people to live in, not including recreational vehicles, boats, and other simlar enterable vehicles.

Properties that are not strictly commercial nor residential (home garages being the most common) which are attached to or immediately adjoining a residential property on the same piece of land and owned by the same person fall under this requirement despite not being strictly residential (livible). This is because in reality, if you were in your house next to the garage and it was broken into, you would likely hear the glass smash, or crashing, etc.

**Requirements:**

#. Property owner must be online.
#. Minimum law enforcement available: 2

Commercial Property
-------------------
Commercial property such as storefronts, offices, businesses, warehouses, and other non-residential properties, may be broken into and burgalarized, even when the owner is not online. At least two law enforcement must be available to respond.

**Requirements:**

#. Minimum law enforcement available: 2

Faction & Government Property
-----------------------------
Since there is no owner for faction or government properties, only the requisite amount of law enforcement must be available.

**Requirements:**

#. Minimum law enforcement available: 2

Warrants & Property Inspections
-------------------------------
Law enforcement and emergency services personnel may enter properties without the owner needing to be online with a sufficient enough reason such as serving a search or arrest warrant, entering the property with exigent circumstances, performing a fire inspection, etc.
  
Character Robberies
-------------------
You may not player kill someone just before, during, or directly after a petty robbery. This is to prevent abuse of the amnesia effect when normally violence would not be utilized. A player would be excempt from this rule if there was clear intent that the PK of the victim was not to use the amnesia effect, but rather to protect themselves (victim disregarding their life, taking extra steps to get you caught) or exterior motive (intent to kill due to previous encounters). In the end it is up to the handling administrators best judgement to determine if the PK was being used to maliciously abuse the amnesia effect.
  
Vehicle Robbery
---------------
Vehicle robbery means vehicles which are broken into and have the contents inside stolen. They do not require the owner to be online and are treated like faction or government property robberies, however, government vehicles require permission from an administrator to be broken into.

Vehicle Theft
-------------
Vehicle theft refers to actually taking and moving the vehicle in question. This may only be done or attempted once every 24 hours. Just like robbery, government vehicles require permission from an administrator. An exception to this is a spontaneous vehicle pursuit and the government vehicle in question is co-opted.

If the vehicle is left unlocked/opened, you may steal the car without admin permission.

You cannot make a "copy" of any keys.

If a player timed out/crashed you may not steal the vehicle, an administrator can check connection logs to determine if the player timed out.
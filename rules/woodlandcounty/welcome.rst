###########################
Welcome to Woodland County!
###########################

.. _Upper Administration Team: https://forums.priorityzerogaming.com/support/
.. _Administration Team: https://forums.priorityzerogaming.com/support/
.. _Development Team: https://forums.priorityzerogaming.com/support/
.. _Support Portal: https://forums.priorityzerogaming.com/support/
.. _idea tracker: https://forums.priorityzerogaming.com/ideas/
.. _bug tracker: https://forums.priorityzerogaming.com/bugs/

Woodland County is a modern county town in Second Life which aims to provide the base of where the country meets the city.

Woodland County currently consists of the following official sim(s):

#. **Lightning**: A modern town with residential, commercial and government services.
#. **Diamondback Lake**: A small lake with light residential and government services.
#. **Isolated Heights**: A upper class residential and light commercial area.

Enforcement is separated into two categories:

* **Rules/Regulations** - Rules & Regulations of the sim, these are `Administration Team`_ enforced and do result in warnings, ejects and bans from Woodland County sims. Rules will **always** override Laws.
* **Laws** - Laws which are provided for your information and should be abided by, failure to do so will result with in-character law enforcement action. Severe breaking of laws may result in Administration intervention.

Common Definitions:

* **Player**: A player is defined as the OOC controller of a character. A player may have more than one character. Players are subject to OOC rules. Players must always remain OOC, and must keep their OOC emotions from mixing into IC character.
* **Character**: A character is defined the playable character whose actions are directly controlled by the Player. Other names for this may be avatar. Characters are subject to IC rules and laws.
* **Megagaming**: See roleplay rules.
* **Powergaming**: See roleplay rules.
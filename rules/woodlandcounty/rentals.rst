#############
Rental Rules
#############
.. note::

  These are the rental rules which are in place to maintain order and stability in the server.

.. _Upper Administration Team: https://forums.priorityzerogaming.com/support/
.. _Administration Team: https://forums.priorityzerogaming.com/support/
.. _Development Team: https://forums.priorityzerogaming.com/support/
.. _Support Portal: https://forums.priorityzerogaming.com/support/
.. _idea tracker: https://forums.priorityzerogaming.com/ideas/
.. _bug tracker: https://forums.priorityzerogaming.com/bugs/

General Provisions
===================
Renting is a luxury within Woodland County, it provides you a means to support the community and receive something in return for donating.

Renting can provide you in some cases an OOC place to relax and have downtime, give you an place to enhance your roleplay and much more.

.. important:: 

  In order to be fair: renting does **not** exempt you from any rules, and you will not be entitled to a refund if you are punished or banned.

No Refunds
-------------------
Rent paid is non-refundable. Unless otherwise decided by the Upper Administration Team.

Residential Rentals
===================
Residential rentals are solely for the purpose of residing in.

The following restrictions are enforced:

#. Residential rentals may not be used for commercial use without permission from the Upper Administration Team.
#. Residential lots are OOC by default, unless expressly made IC by the tenant by a sign out the front of the property.
#. Residential lots have additional IC rules attached to them to prevent abuse by others.
#. Residential lots can be made IC by a sign out the front. If you do not have a sign, you may request one via the appropriate method.


##############
Faction Rules
##############

.. note::

  These are the faction/organization rules which are in place to maintain fairness and order in the server.
  
.. _Upper Administration Team: https://forums.priorityzerogaming.com/support/
.. _Administration Team: https://forums.priorityzerogaming.com/support/
.. _Development Team: https://forums.priorityzerogaming.com/support/
.. _Support Portal: https://forums.priorityzerogaming.com/support/
.. _idea tracker: https://forums.priorityzerogaming.com/ideas/
.. _bug tracker: https://forums.priorityzerogaming.com/bugs/

Faction Wars
============

Starting & Ending a War
-----------------------
If a feud occurs between factions, they may seek approval for a faction war from the `Administration Team`_. This approval is sought when multiple Character Kills are expected and the goal is to debilitate or eradicate the opposing faction(s). Faction wars may be ended if all factions involved in the war agree to a truce or at any time the `Administration Team`_ deems necessary.

Character-Killing
-----------------
After a faction war is approved, every violent conflict is considered a character-kill situation for the faction members, associates, assistants and perceived members. Admin supervision should be requested wherever possible to prevent any disputes. Any character kills or confrontations regarding the faction war should be reported to the `Administration Team`_ so they may keep track of the feud.

Alternate Characters
--------------------
No alternate characters from the factions or players involved may be allowed at any time with the exception of approved leadership alts. New or low hour characters may not be created or used during the faction war.

Use of Government Perks
=======================
Any faction that is financially supported through the `Administration Team`_ or Scripting may not have a wage higher than $1,500 except for brief periods of time where bonuses may be given out for the holidays.

Corruption
==========
Corruption in government factions is restricted unless a set of parameters is created and submitted to the `Upper Administration Team`_ for approval. After approval, the Administration Team may delegate according to the parameters who has corruption. 

Financial corruption such as embezzling money is not allowed under any circumstances unless `Upper Administration Team`_ approval is given.

Faction Recruitment
===================
In the event someone is not able to continue roleplaying in their faction permanently, such as being imprisoned for life or killed, they must wait at least 72 hours before rejoining the faction in any capacity. If the faction is rejoined the player must wait 14 days at minimum before attaining one rank below the previously held rank.

Exceptions for this may be made by contacting the `Administration Team`_, especially regarding faction leadership ranks.

Recruitment Process Rules
-------------------------
The following restrictions are applied to all factions recruitment process:

#. Minimum age requirements are forbidden (some characters may use alt accounts).
#. Applications should be responded (accepted/rejected) within 12 hours, and a maximum of 72 hours. It is suggested faction leaders designate a recruitment delegate for the handling of applications.

Multiple failures to comply with this may result in loss of leadership.

Alternate Characters
====================
Faction leaders may obtain permission from the `Administration Team`_ to have a single alternate character in their faction at a time. This character may not hold any supervisory position.

Faction Shutdown
================
In the event that a faction, both legal or illegal succumb to inactivity, all faction leaders must agree on the same plan of action if the following events occur;

* Any rental is modified or deleted.
* A voluntary shutdown is set into motion.

Area Activity
=============
Factions should make an effort to keep their roleplay areas active. This means the interiors should be used by the faction and roleplayed with or an attempt to do such must be made. Sitting on an area and hoarding them may get them force sold. These areas are evaluated on a case by case basis by the `Administration Team`_.

This does not include rental areas that players rent.
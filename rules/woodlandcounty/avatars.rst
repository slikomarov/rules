
Avatar & Character Rules
########################
.. note::

  These are the Avatar & Character rules which are in place to maintain the environment, theme and feel of the server.

  **Avatar is described as the apperance of the character.**

.. _Upper Administration Team: https://forums.priorityzerogaming.com/support/
.. _Administration Team: https://forums.priorityzerogaming.com/support/
.. _Development Team: https://forums.priorityzerogaming.com/support/
.. _Support Portal: https://forums.priorityzerogaming.com/support/
.. _idea tracker: https://forums.priorityzerogaming.com/ideas/
.. _bug tracker: https://forums.priorityzerogaming.com/bugs/


:Potential Punishment: 3 day suspension or removal from the community

Allowed Avatars Types
=====================
The following avatars with the following look or apperance are permitted:

- Human avatars are permitted
- Furry avatars are permitted with restrictions
- Feral avatars are permitted with restrictions
- Child avatars require approval and have restrictions

Theme Appropriate
-----------------
All avatars in Woodland County which are permitted, must be theme and context appropriate. I.E. You should dress how someone would in the current year, not 1920's.

If your avatar is not theme appropriate, you may be asked to change it or ejected from the sim until you have corrected it.

If you are unsure if your avatar is theme appropriate, please send a screenshot and seek approval from the `Administration Team`_.

Furry Avatars
-------------
Furry avatars are described as animals which behave and act anthromorphically.

.. note::

  Furry avatars make up a major part of the Second Life Community with their avatars, as such we have chosen to allow such avatars however have expectations in how they behave laid out below.


Expected Behavour
"""""""""""""""""
Furry avatars are considered on the same level as human avatars and are expected to be somewhat realistic and wear theme appropriate attire.

Feral/Animal Avatars
--------------------
Feral avatars are described as animals which behave and act as animals. This can include domesticated animals.

Species Appropriate
"""""""""""""""""""
Feral avatars must be the appropriate species to the north-eastern area of the United States.

Expected Behavour
"""""""""""""""""
Feral avatars are expected to behave as their chosen animal, they can interact as they would, but cannot speak at all or show unrealistic intelligence. 

Child Avatars
-------------
Child avatars are described as being younger than 16 years of age and must act as such.

These avatars require approval from the `Upper Administration Team`_ before being played.

Expected Behavour
"""""""""""""""""
Child avatars are expected to behave as such, and cannot 

Verified Account
""""""""""""""""
An additional requirement is the player must have "payment info used" or an account verified as deemed by Linden Labs.

No Adult Actions
""""""""""""""""
Child avatars cannot engage in any adult actions. Including but not limited to: Sexual interactions, entering adult locations (like bars, porn shops, stripclubs, etc).

No Crimes
"""""""""
Child avatars cannot engage in any major criminal activities

Juvenile Acts
^^^^^^^^^^^^^
An exemption to the above is juvenile acts which can expect IC repercussions.

Limited Driving
^^^^^^^^^^^^^^^
Child avatars should limit driving any vehicle which can expect IC repercussions.

Avatar Restrictions
===================
The following restrictions apply to all avatars.

:Potential Punishment: Ejected, 3 day suspension

Macro Avatars
-------------
Macro avatars or those over 2.5 meters/8 feet are not permitted.

Micro Avatars
-------------
Micro avatars or thosr under 1.5 meters/5 feet are not permitted.

Exception
"""""""""
An exception to this rule is child avatars and feral avatars.

Emergency Service & Military Style Uniforms
-------------------------------------------
Only official Woodland County groups or unless approved by the `Administration Team`_ may wear emergency service or military styled uniforms.

This is to reduce confusions with players.


Forbidden Avatars
-----------------
Avatars with the following look or apperance are explicitly forbidden:

- Feral Dragons

Forbidden Abilities
-------------------
Avatars are not permitted possess or have any of the below abilities:

- Superhuman Powers
- Superpowers
- In-human Traits
- Vampires


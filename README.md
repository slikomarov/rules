# Priority Zero Gaming Rules [![Documentation Status](https://readthedocs.org/projects/priority-zero-gaming-rules/badge/?version=latest)](https://priority-zero-gaming-rules.readthedocs.io/en/latest/?badge=latest)

All Priority Zero Gaming Network Rules.

This can be found at https://rules.priorityzerogaming.net/

If working on locally you can build the environment by using `sphinx-autobuild`

### Windows

```
pip install -r requirements.txt
pip install sphinx-autobuild
make html
```

### Linux/MacOS

```
pip install -r requirements.txt
pip install sphinx-autobuild
./Makefile html
```
